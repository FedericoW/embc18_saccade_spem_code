function [signals, LSSM, saccParamsEstim] = model_based_saccade_spem_detection(plantModelType,saccModelType,...
                                                                     spemModelType,signals,varargin)

% MODEL_BASED_SACCADE_SPEM_DETECTION 

p = inputParser;
addOptional(p, 'saccParamsTrue', [], @isnumeric);
addOptional(p, 'V_U_updates', [], @isnumeric);
addOptional(p, 'U_priors', []);
addOptional(p, 'U_prior_args', [], @isnumeric);
addOptional(p, 'V_Z_init', 0.5^2, @isnumeric);
addOptional(p, 'V_Z_update', true, @islogical);
addOptional(p, 'V_Z_prior', 'no_prior');
addOptional(p, 'V_Z_prior_args', [], @isnumeric);
addOptional(p, 'V_W_init', [], @isnumeric);
addOptional(p, 'V_W_updates', [], @isnumeric);
addOptional(p, 'V_W_prior', 'jeffreys_prior');
addOptional(p, 'V_W_prior_args', [], @isnumeric);
addOptional(p, 'verbose', false, @islogical);
parse(p,varargin{:});


% Load LSSM:
LSSM = load_LSSM(plantModelType, saccModelType, spemModelType, signals);

% Sparse input estimation/signal separation:
LSSM.V_Z = p.Results.V_Z_init;
LSSM.V_W = diag(p.Results.V_W_init);

EM_steps = 20; % Number of EM iterations

[signals.u_hat, ~, signals.x_hat, ~, LSSM] = EM_LSSM(signals.data, LSSM, EM_steps, ...
                                                     'U_priors', p.Results.U_priors,...
                                                     'U_prior_args', p.Results.U_prior_args,...
                                                     'V_W_updates', p.Results.V_W_updates, ...
                                                     'V_W_prior', p.Results.V_W_prior, ...
                                                     'V_W_prior_args', p.Results.V_W_prior_args, ...
                                                     'V_Z_update', p.Results.V_Z_update, ...
                                                     'V_Z_prior', p.Results.V_Z_prior, ...
                                                     'verbose', p.Results.verbose);

disp(['sigma_Z_estim: ' num2str(sqrt(LSSM.V_Z))])
disp(['sigma_W_estim: ' num2str(sqrt(diag(LSSM.V_W)'))])

% Collect estimated signals:
signals = collect_signals(signals, LSSM);

% Get saccade parameters:
saccParamsEstim = get_saccade_params(signals.y_spemfree, ...
                                     signals.dy_spemfree, ...
                                     signals.ddy_spemfree, ...
                                     signals.time);
                                 
% Estimated eye movement type (fixation, saccade or SPEM):
signals.class_estim = get_signal_class(saccParamsEstim, signals.dy_spem, 1/signals.samplingRate);


end

