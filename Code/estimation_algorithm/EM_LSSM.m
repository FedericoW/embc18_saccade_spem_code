function [u_hat, y_tld_hat, x_hat, log_likelihood, LSSM] = EM_LSSM(y, LSSM, maxsteps, varargin)

%EM_LSSM Estimate variances (input, state noise and measurement noise) via
%expectation maximization algorithm

p = inputParser;
addOptional(p, 'V_U_updates', 1:LSSM.udim, @isnumeric);
addOptional(p, 'U_priors',num2cell(repmat('jeffreys_prior', LSSM.udim, 1), 2));
addOptional(p, 'U_prior_args', [], @isnumeric);
addOptional(p, 'V_Z_update', true, @islogical);
addOptional(p, 'V_Z_prior', 'no_prior');
addOptional(p, 'V_Z_prior_args', [], @isnumeric);
addOptional(p, 'V_W_updates', 1:LSSM.xdim, @isnumeric);
addOptional(p, 'V_W_prior', 'jeffreys_prior');
addOptional(p, 'V_W_prior_args', [], @isnumeric);
addOptional(p, 'verbose', false, @islogical);
parse(p,varargin{:});

log_likelihood = zeros(1, maxsteps+1);

EM_count = 1;
while(EM_count <= maxsteps)
    if p.Results.verbose
        disp('EM step:')
        disp(EM_count)
    end
    
    % MBF Kalman smoother forward pass
    [msg, log_likelihood(EM_count)] = MBF_fwd(LSSM, y);

    % MBF Kalman smoother backward pass
    msg = MBF_bwd(LSSM, y, msg);
    
    % Second moments for Sigma_Z and Sigma_W updates
    V_X_X = zeros(LSSM.xdim);
    V_Xm1_Xm1 = zeros(LSSM.xdim);
    V_Xm1_X = zeros(LSSM.xdim);
    V_Xm1_U = zeros(LSSM.xdim, LSSM.udim);
    V_X_U = zeros(LSSM.xdim, LSSM.udim);
    V_U_U = zeros(LSSM.udim);
    
    for k=1:LSSM.lenRecording
        if k==1
            V_Xm1_X = V_Xm1_X + LSSM.V_X0 * LSSM.A' * (eye(LSSM.xdim) ...
                - msg.mrg_tld_X.tld_W(:,:,1) * msg.fwd_tld_X.V(:,:,1)) ...
                + LSSM.m_X0 * msg.mrg_X.m(:,1)';
            
            V_Xm1_U = V_Xm1_U - LSSM.V_X0 * LSSM.A' ...
                * msg.mrg_tld_X.tld_W(:,:,1) * LSSM.B * LSSM.V_U(:,:,1) ...
                + LSSM.m_X0 * msg.mrg_U.m(:,k)';
        else
            V_Xm1_X = V_Xm1_X + msg.F(:,:,k-1) * msg.fwd_tld_X.V(:,:,k-1) ...
                * LSSM.A' * (eye(LSSM.xdim) - msg.mrg_tld_X.tld_W(:,:,k) ...
                * msg.fwd_tld_X.V(:,:,k)) ...
                + msg.mrg_X.m(:,k-1) * msg.mrg_X.m(:,k)';
        
            V_Xm1_U = V_Xm1_U - msg.F(:,:,k-1) * msg.fwd_tld_X.V(:,:,k-1) ...
                * LSSM.A' * msg.mrg_tld_X.tld_W(:,:,k) * LSSM.B * LSSM.V_U(:,:,k) ...
                + msg.mrg_X.m(:,k-1) * msg.mrg_U.m(:,k)';
        end
        
        V_U_U = V_U_U + msg.mrg_U.V(:,:,k) ...
            + msg.mrg_U.m(:,k) * msg.mrg_U.m(:,k)';
        
        V_X_U = V_X_U + (eye(LSSM.xdim) - msg.fwd_tld_X.V(:,:,k) ...
            * msg.mrg_tld_X.tld_W(:,:,k)) * LSSM.B * LSSM.V_U(:,:,k) ...
            + msg.mrg_X.m(:,k) * msg.mrg_U.m(:,k)';
        
        V_X_X = V_X_X + msg.mrg_X.V(:,:,k) ...
            + msg.mrg_X.m(:,k) * msg.mrg_X.m(:,k)';
        
        if k==LSSM.lenRecording - 1
            V_Xm1_Xm1 = V_X_X + LSSM.V_X0 + LSSM.m_X0 * LSSM.m_X0';
        end
    end
    
    % Measurement noise sigma_Z update:
    if p.Results.V_Z_update
        val = 1 / (LSSM.lenRecording) * (norm(y)^2 - 2 * LSSM.C * msg.mrg_X.m * y' ...
            + LSSM.C * V_X_X * LSSM.C');
        if strcmp(p.Results.V_Z_prior, 'no_prior')
            LSSM.V_Z = val;
        elseif strcmp(p.Results.V_Z_prior, 'jeffreys_prior')
            LSSM.V_Z = val / 3;
        end
    end
    
    % State noise sigma_W update:
    for d=p.Results.V_W_updates
        val = 1 / (LSSM.lenRecording) ...
            * (V_X_X(d,d) + LSSM.A(d,:) * V_Xm1_Xm1 * LSSM.A(d,:)' ...
            + LSSM.B(d,:) * V_U_U * LSSM.B(d,:)' ...
            - 2 * LSSM.A(d,:) * V_Xm1_X(:,d) ...
            + 2 * LSSM.A(d,:) * V_Xm1_U * LSSM.B(d,:)' ...
            - 2 * V_X_U(d,:) * LSSM.B(d,:)');
        if strcmp(p.Results.V_W_prior, 'no_prior')
            LSSM.V_W(d,d) = val;
        elseif strcmp(p.Results.V_W_prior, 'jeffreys_prior')
            LSSM.V_W(d,d) = val / 3;
        end
    end
    
    % Input variance sigma_U update:
    for iu=p.Results.V_U_updates
        if strcmp(p.Results.U_priors{iu}, 'jeffreys_prior')
            % MAP update with jeffrey's:
             for k=1:LSSM.lenRecording
                 log_likelihood(EM_count) = log_likelihood(EM_count) ...
                     - log(LSSM.V_U(iu,iu,k));
                 LSSM.V_U(iu,iu,k) = (msg.mrg_U.m(iu,k)^2 + msg.mrg_U.V(iu,iu,k))/3;
             end

        elseif strcmp(p.Results.U_priors, 'invgamma_prior')
            % MAP update with inverse-gamma prior:
            alpha = p.Results.U_prior_args(iu,1);
            beta = p.Results.U_prior_args(iu,2);
            
            for k=1:LSSM.lenRecording
                log_likelihood(EM_count) = log_likelihood(EM_count) - (alpha+1) * log(LSSM.V_U(iu,iu,k)) - beta / LSSM.V_U(iu,iu,k);
                LSSM.V_U(iu,iu,k) = (msg.mrg_U.m(iu,k)^2 + msg.mrg_U.V(iu,iu,k) + 2*beta) / (2*alpha + 3);
            end
        end
        EM_count = EM_count + 1;
    end

% MBF forward pass (Kalman filter):
[msg, log_likelihood(EM_count)] = MBF_fwd(LSSM, y);

% MBF backward pass (Kalman smoother step):
msg = MBF_bwd(LSSM, y, msg);
u_hat = msg.mrg_U.m;
x_hat = msg.mrg_X.m;
y_tld_hat = msg.mrg_tld_Y.m;
end

