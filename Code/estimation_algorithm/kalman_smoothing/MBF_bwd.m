function [msg] = MBF_bwd(LSSM, y, msg)
%Backward message passing of Modified Bryson-Frazier algorithm
%   Inputs
%   @model: Contains specifications of state space model
%   @y: Given observation
%   @fwd_X: Forward messages of X_k's after observation y_k
%   (calculated with MBF_fwd)
%   @fwd_tld_X: Forward messages of X_k's before observation y_k
%   (calculated with MBF_fwd)
%   Outputs
%   @mrg_U: Posterior messages of inputs U_k's
%   @mrg_tld_Y: Posterior messages of noise-free outputs y_k's
%   @mrg_X: Posterior messages of states X_k's

A = LSSM.A;
B = LSSM.B;
C = LSSM.C;
xdim = LSSM.xdim;
udim = LSSM.udim;
ydim = LSSM.ydim;
fwd_X = msg.fwd_X;
fwd_tld_X = msg.fwd_tld_X;

% Initialize output messages:
mrg_X.m = zeros(xdim, LSSM.lenRecording);
mrg_X.V = zeros(xdim, xdim, LSSM.lenRecording);

mrg_U.m = zeros(udim, LSSM.lenRecording);
mrg_U.V = zeros(udim, udim, LSSM.lenRecording);

mrg_tld_Y.m = zeros(ydim, LSSM.lenRecording);
mrg_tld_Y.V = zeros(ydim, ydim, LSSM.lenRecording);

bwd_U.m = zeros(udim, LSSM.lenRecording);
bwd_U.V = zeros(udim, udim, LSSM.lenRecording);

% Initialize other messages:
mrg_X.zeta = zeros(xdim, LSSM.lenRecording);
mrg_X.tld_W = zeros(xdim, xdim, LSSM.lenRecording);

mrg_tld_X.zeta = zeros(xdim, LSSM.lenRecording);
mrg_tld_X.tld_W = zeros(xdim, xdim, LSSM.lenRecording);

mrg_U.zeta = zeros(udim, LSSM.lenRecording);
mrg_U.tld_W = zeros(udim, udim, LSSM.lenRecording);

m_U = zeros(LSSM.udim, LSSM.lenRecording);
m_U(:,1:LSSM.lenRecording) = LSSM.m_U;
V_U = zeros(LSSM.udim, LSSM.udim, LSSM.lenRecording);
V_U(:,:,1:LSSM.lenRecording) = LSSM.V_U;

F = zeros(xdim, xdim, LSSM.lenRecording);

% Backward message passing:
for k=LSSM.lenRecording:-1:1
    G_k = (LSSM.V_Z + C * fwd_tld_X.V(:,:,k) * C')^(-1);
    F(:,:,k) = eye(xdim, xdim) - fwd_tld_X.V(:,:,k) * C' * G_k * C;
    mrg_tld_X.zeta(:,k) = F(:,:,k)' * mrg_X.zeta(:,k) ...
        + C' * G_k * (C * fwd_tld_X.m(:,k) - y(:,k));
    mrg_tld_X.tld_W(:,:,k) = F(:,:,k)' * mrg_X.tld_W(:,:,k) * F(:,:,k) + C' * G_k * C;
    
    if k==1
        mrg_X0.zeta = A' * mrg_tld_X.zeta(:,k);
        mrg_X0.tld_W = A' * mrg_tld_X.tld_W(:,:,k) * A;
        
        % Posterior message of initial state X_0:
        mrg_X0.m = LSSM.m_X0 - LSSM.V_X0 * mrg_X0.zeta;
        mrg_X0.V = LSSM.V_X0 ...
            - LSSM.V_X0 * mrg_X0.tld_W * LSSM.V_X0;
    else
        mrg_X.zeta(:,k-1) = A' * mrg_tld_X.zeta(:,k);
        mrg_X.tld_W(:,:,k-1) = A' * mrg_tld_X.tld_W(:,:,k) * A;
    end
    
    mrg_U.zeta(:,k) = B' * mrg_tld_X.zeta(:,k);
    mrg_U.tld_W(:,:,k) = B' * mrg_tld_X.tld_W(:,:,k) * B;
    
    % Posterior message X_k:
    mrg_X.m(:,k) = fwd_X.m(:,k) - fwd_X.V(:,:,k) * mrg_X.zeta(:,k);
    mrg_X.V(:,:,k) = fwd_X.V(:,:,k) ...
        - fwd_X.V(:,:,k) * mrg_X.tld_W(:,:,k) * fwd_X.V(:,:,k);
    
    % Posterior message U_k:
    mrg_U.m(:,k) = m_U(:,k) - V_U(:,:,k) * mrg_U.zeta(:,k);
    mrg_U.V(:,:,k) = V_U(:,:,k) ...
        - V_U(:,:,k) * mrg_U.tld_W(:,:,k) * V_U(:,:,k);
    
    % Posterior message tld_Y_k:
    mrg_tld_Y.m(:,k) = C * mrg_X.m(:,k);
    mrg_tld_Y.V(:,:,k) = C * mrg_X.V(:,:,k) * C';
end

msg.mrg_U = mrg_U;
msg.mrg_tld_Y = mrg_tld_Y;
msg.mrg_X = mrg_X;
msg.bwd_U = bwd_U;
msg.mrg_X0 = mrg_X0;
msg.mrg_tld_X = mrg_tld_X;
msg.F = F;

end

