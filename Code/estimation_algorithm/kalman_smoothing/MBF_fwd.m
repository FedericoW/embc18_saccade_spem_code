function [msg, log_likelihood] = MBF_fwd(LSSM, y)
%Forward message passing of Modified Bryson-Frazier algorithm
%   Inputs
%   @model: Contains specifications of state space model
%   @y: Given observation
%   Outputs
%   @fwd_X: Forward messages of X_k's after observation y_k
%   @fwd_tld_X: Forward messages of X_k's before observation y_k
%   @log_likelihood: log-likelihood of observation y

% check if state noise is defined in LSSM:
if ~isfield(LSSM, 'V_W')
    LSSM.V_W = zeros(LSSM.xdim);
end

A = LSSM.A;
B = LSSM.B;
C = LSSM.C;
xdim = LSSM.xdim;

% Initalize outputs:
fwd_X.m = zeros(xdim, LSSM.lenRecording);
fwd_X.V = zeros(xdim, xdim, LSSM.lenRecording);

fwd_tld_X.m = zeros(xdim, LSSM.lenRecording);
fwd_tld_X.V = zeros(xdim, xdim, LSSM.lenRecording);

m_U = zeros(LSSM.udim, LSSM.lenRecording);
m_U(:,1:LSSM.lenRecording) = LSSM.m_U;
V_U = zeros(LSSM.udim, LSSM.udim, LSSM.lenRecording);
V_U(:,:,1:LSSM.lenRecording) = LSSM.V_U;

log_likelihood = 0;

% Forward message passing:
for k=1:LSSM.lenRecording
    if k==1
        fwd_tld_X.m(:,k) = A * LSSM.m_X0 + B * m_U(:,k);
        fwd_tld_X.V(:,:,k) = A * LSSM.V_X0 * A' ...
            + B * V_U(:,:,k) * B' + LSSM.V_W;
    else
        fwd_tld_X.m(:,k) = A * fwd_X.m(:,k-1) + B * m_U(:,k);
        fwd_tld_X.V(:,:,k) = A * fwd_X.V(:,:,k-1) * A' ...
            + B * V_U(:,:,k) * B' + LSSM.V_W;
    end
        
    G_k = (LSSM.V_Z + C*fwd_tld_X.V(:,:,k)*C')^(-1);
    alpha_k = y(:,k) - C*fwd_tld_X.m(:,k);
    fwd_X.m(:,k) = fwd_tld_X.m(:,k) + fwd_tld_X.V(:,:,k) * C' * G_k * alpha_k;
    fwd_X.V(:,:,k) = fwd_tld_X.V(:,:,k) ...
        - fwd_tld_X.V(:,:,k) * C' * G_k * C * fwd_tld_X.V(:,:,k);
    
    log_likelihood = log_likelihood ...
        - 0.5*(log(2 * pi * det(G_k)^(-1)) + alpha_k' * G_k * alpha_k);
end

msg.fwd_X = fwd_X;
msg.fwd_tld_X = fwd_tld_X;

end

