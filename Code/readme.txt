Revelant papers for implementation


1.) Oculomotor plant model:
Bahill, A. Terry, Jose R. Latimer, and B. Todd Troost.
"Linear homeomorphic model for human movement." 
IEEE Transactions on Biomedical Engineering 11 (1980): 631-639.


2.) Saccade main sequence parameters:
Bahill, A. Terry, Michael R. Clark, and Lawrence Stark. 
"The main sequence, a tool for studying human eye movements."
Mathematical Biosciences 24.3-4 (1975): 191-204.


3.) Kalman filtering and smoothing algorithm:
Loeliger, H. A., Dauwels, J., Hu, J., Korl, S., Ping, L., and Kschischang, F. R.  
"The factor graph approach to model-based signal processing." 
Proceedings of the IEEE 95.6 (2007): 1295-1322.

4.) Expectation Maximization algorithm and sparsity:
Loeliger, H. A., Bruderer, L., Malmberg, H., Wadehn, F., and Zalmai, N.
"On sparsity by NUV-EM, Gaussian message passing, and Kalman smoothing.
IEEE Information Theory and Applications Workshop (ITA), 2016.