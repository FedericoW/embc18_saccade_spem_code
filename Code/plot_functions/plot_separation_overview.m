function plot_separation_overview(signals)

time = 1000*signals.time;
t_max = max(time);

subplot(3,1,1)
plot(time, signals.data, 'Color', [0.6 0.8 0.4], 'LineWidth', 4)
hold on
plot(time, signals.y_hat, 'Color', [0.1 0.1 0.1], 'LineWidth', 1)

ylabel('$\theta \; [^{\circ}]$', 'Interpreter', 'latex')
lgd = legend('Data', 'Estimated position');
set(lgd, 'Interpreter', 'latex')
set(gca,'FontSize',13)
hold off
axis([0 t_max -15 20])


subplot(3,1,2)
stairs(time, signals.control_spemfree, 'Color', [0.1 0.1 0.1], 'LineWidth', 1)
ylabel('$y^{Pulse}$ [g]', 'Interpreter', 'latex')
lgd = legend('Estimated pulse-step signal');
set(lgd, 'Interpreter', 'latex')
set(gca,'FontSize',13)
axis([0 t_max -100 150])


subplot(3,1,3)
plot(time, signals.control_spem, 'Color', [0.1 0.1 0.1], 'LineWidth', 1)
xlabel('Time [ms]', 'Interpreter', 'latex')
ylabel('$y^{SPEM}$ [g]', 'Interpreter', 'latex')
lgd = legend('Estimated SPEM signal');
set(lgd, 'Interpreter', 'latex')
set(gca,'FontSize',13)
axis([0 t_max -50 80])

linkaxes(findobj(gcf,'type','axes'),'x')

end


