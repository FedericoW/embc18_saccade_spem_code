function plot_spem_analysis(signals, LSSM, spemModelType, a_spem, f_spem)

t_max = max(signals.time);

if strcmp(spemModelType, 'integratedRW')
    
    hold on

    plot(signals.time, signals.dy_spem, 'Color', [0.1 0.1 0.1], 'LineWidth', 1)    
    plot(signals.time, signals.dtarget, 'k:')
    
    xlabel('Time [s]', 'Interpreter', 'latex')
    ylabel('$\dot\theta \; [^{\circ}/s]$', 'Interpreter', 'latex')
    lgd = legend('Estimated SPEM velocity', 'Target SPEM velocity');
    set(lgd, 'Interpreter', 'latex')
    set(gca,'FontSize',13)
    axis([0 t_max -50 80])
    hold off
    
elseif strcmp(spemModelType, 'integratedSine')

    subplot(3,1,1)
    hold on
    
    plot(signals.time, signals.dy_spem, 'Color', [0.1 0.1 0.1], 'LineWidth', 1);
    plot(signals.time, signals.dtarget, 'k:');
    
    ylabel('$\dot\theta \; [^{\circ}/s]$', 'Interpreter', 'latex')
    lgd = legend('Estimated SPEM velocity', 'Target SPEM velocity');
    set(lgd, 'Interpreter', 'latex')
    set(gca,'FontSize',13)
    axis([0 t_max -5-2*pi*f_spem*a_spem 20+2*pi*f_spem*a_spem])
    hold off

    
    spem_sin = lsim(LSSM.plant_model_cont, ...
        LSSM.C_spem_sin * signals.x_hat, signals.time);
    spem_cos = lsim(LSSM.plant_model_cont, ...
        LSSM.C_spem_cos * signals.x_hat, signals.time);
    
    
    subplot(3,1,2)
    hold on
    
    plot(signals.time, sqrt(spem_sin.^2 + spem_cos.^2), 'Color', [0.1 0.1 0.1], 'LineWidth', 1)
    plot(signals.time, 2*pi*f_spem*a_spem*ones(length(signals.time), 1), 'k:')
    
    lgd = legend('Estimated sin amplitude', 'Target sine amplitude');
    set(lgd, 'Interpreter', 'latex')
    set(gca,'FontSize',13)
    axis([0 t_max -1 20+2*pi*f_spem*a_spem])
    hold off
    
    
    subplot(3,1,3)
    hold on
    plot(signals.time, atan(spem_sin ./ spem_cos), 'Color', [0.1 0.1 0.1], 'LineWidth', 1)
    plot(signals.time, atan(cos(2*pi*f_spem*(signals.time-max(signals.time)/2)) ./ ...
        -sin(2*pi*f_spem*(signals.time-max(signals.time)/2))), 'k:')
    
    axis([0 t_max -2 4])
    xlabel('Time [s]', 'Interpreter', 'latex')
    lgd = legend('Estimated sin phase', 'Target sine phase');
    set(lgd, 'Interpreter', 'latex')
    set(gca,'FontSize',13)
    hold off
end

end

