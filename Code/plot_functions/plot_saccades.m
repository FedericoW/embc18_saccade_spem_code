function plot_saccades(signals, saccade_params_true,saccade_params_estim, dataSource, saccGenerationModel)

time = 1000*signals.time;

t_max = max(time);

% Position plot:
subplot(3,1,1)
hold on
plot(time, signals.data, 'Color', [0.6 0.8 0.4], 'LineWidth', 1)
if strcmp(dataSource, 'generated')
    plot(time, signals.theta_sacc, 'Color', [0.8 0.2 0.2], 'LineWidth', 1)
end

plot(time, signals.y_spemfree, 'Color', [0.1 0.1 0.1], 'LineWidth', 1)

if strcmp(dataSource, 'generated')
    for i=1:length(saccade_params_true)
        plot(time(saccade_params_true{i}.start_index), ...
            signals.theta_sacc(saccade_params_true{i}.start_index), 'o', ...
            'Color', [0.8 0.2 0.2], 'LineWidth', 1)
        plot(time(saccade_params_true{i}.v_max_index), ...
            signals.theta_sacc(saccade_params_true{i}.v_max_index), 'x', ...
            'Color', [0.8 0.2 0.2], 'LineWidth', 1)
        plot(time(saccade_params_true{i}.end_index), ...
            signals.theta_sacc(saccade_params_true{i}.end_index), 'v', ...
            'Color', [0.8 0.2 0.2], 'LineWidth', 1)
    end
    
elseif strcmp(dataSource, 'Andersson2013')
    for i=1:length(saccade_params_true)
        plot(time(saccade_params_true{i}.start_index), ...
            signals.data(saccade_params_true{i}.start_index), 'o', ...
            'Color', [0.6 0.8 0.4], 'LineWidth', 1)
        plot(time(saccade_params_true{i}.end_index), ...
            signals.data(saccade_params_true{i}.end_index), 'v', ...
            'Color', [0.6 0.8 0.4], 'LineWidth', 1)
    end
end

for i=1:length(saccade_params_estim)
    plot(time(saccade_params_estim{i}.start_index), ...
        signals.y_spemfree(saccade_params_estim{i}.start_index), 'o', ...
        'Color', [0.1 0.1 0.1], 'LineWidth', 1)
    plot(time(saccade_params_estim{i}.v_max_index), ...
        signals.y_spemfree(saccade_params_estim{i}.v_max_index), 'x', ...
        'Color', [0.1 0.1 0.1], 'LineWidth', 1)
    plot(time(saccade_params_estim{i}.end_index), ...
        signals.y_spemfree(saccade_params_estim{i}.end_index), 'v', ...
        'Color', [0.1 0.1 0.1], 'LineWidth', 1)
end

ylabel('$\theta \; [^{\circ}]$', 'Interpreter', 'latex')
if strcmp(dataSource, 'generated')
    lgd = legend('Data', 'Simulated SPEM-free position', 'Estimated SPEM-free position');
else
    lgd = legend('Data', 'Estimated SPEM-free position');
end

axis([0 t_max -30 30])
set(lgd, 'Location','southeast');
set(lgd, 'Interpreter', 'latex')
set(gca,'FontSize',13)
hold off

% Velocity plot:
subplot(3,1,2)
hold on
if strcmp(dataSource, 'generated')
    plot(time, signals.dtheta_sacc, 'Color', [0.8 0.2 0.2], 'LineWidth', 1)
end
plot(time, signals.dy_spemfree, 'Color', [0.1 0.1 0.1], 'LineWidth', 1)

if strcmp(dataSource, 'generated')
    for i=1:length(saccade_params_true)
        plot(time(saccade_params_true{i}.start_index), ...
            signals.dtheta_sacc(saccade_params_true{i}.start_index), 'o', ...
            'Color', [0.8 0.2 0.2], 'LineWidth', 1)
        plot(time(saccade_params_true{i}.v_max_index), ...
            signals.dtheta_sacc(saccade_params_true{i}.v_max_index), 'x', ...
            'Color', [0.8 0.2 0.2], 'LineWidth', 1)
        plot(time(saccade_params_true{i}.end_index), ...
            signals.dtheta_sacc(saccade_params_true{i}.end_index), 'v', ...
            'Color', [0.8 0.2 0.2], 'LineWidth', 1)
    end
end

for i=1:length(saccade_params_estim)
    plot(time(saccade_params_estim{i}.start_index), ...
        signals.dy_spemfree(saccade_params_estim{i}.start_index), 'o', ...
        'Color', [0.1 0.1 0.1], 'LineWidth', 1)
    plot(time(saccade_params_estim{i}.v_max_index), ...
        signals.dy_spemfree(saccade_params_estim{i}.v_max_index), 'x', ...
        'Color', [0.1 0.1 0.1], 'LineWidth', 1)
    plot(time(saccade_params_estim{i}.end_index), ...
        signals.dy_spemfree(saccade_params_estim{i}.end_index), 'v', ...
        'Color', [0.1 0.1 0.1], 'LineWidth', 1)
end

ylabel('$\dot\theta \; [^{\circ}/s]$', 'Interpreter', 'latex')
if strcmp(dataSource, 'generated')
    lgd = legend('Simulated SPEM-free velocity', 'Estimated SPEM-free velocity');
else
    lgd = legend('Estimated SPEM-free velocity');
end
axis([0 t_max -350 350])
set(lgd, 'Interpreter', 'latex')
set(gca,'FontSize',13)
hold off


% Control signal plot:
subplot(3,1,3)
hold on

if strcmp(dataSource, 'generated')
    if strcmp(saccGenerationModel, 'Bahill1980')
        stairs(time, signals.control_signal, 'Color', [0.8 0.2 0.2], 'LineWidth', 1)
    end
end
stairs(time, signals.control_spemfree, 'Color', [0.1 0.1 0.1], 'LineWidth', 1)


if strcmp(dataSource, 'generated')
    if strcmp(saccGenerationModel, 'Bahill1980')
        for i=1:length(saccade_params_true)
            plot(time(saccade_params_true{i}.start_index), ...
                signals.control_signal(saccade_params_true{i}.start_index), 'o', ...
                'Color', [0.8 0.2 0.2], 'LineWidth', 1)
            plot(time(saccade_params_true{i}.v_max_index), ...
                signals.control_signal(saccade_params_true{i}.v_max_index), 'x', ...
                'Color', [0.8 0.2 0.2], 'LineWidth', 1)
            plot(time(saccade_params_true{i}.end_index), ...
                signals.control_signal(saccade_params_true{i}.end_index), 'v', ...
                'Color', [0.8 0.2 0.2], 'LineWidth', 1)
        end
    end
end

for i=1:length(saccade_params_estim)
    plot(time(saccade_params_estim{i}.start_index), ...
        signals.control_spemfree(saccade_params_estim{i}.start_index), 'o', ...
        'Color', [0.1 0.1 0.1], 'LineWidth', 1)
    plot(time(saccade_params_estim{i}.v_max_index), ...
        signals.control_spemfree(saccade_params_estim{i}.v_max_index), 'x', ...
        'Color', [0.1 0.1 0.1], 'LineWidth', 1)
    plot(time(saccade_params_estim{i}.end_index), ...
        signals.control_spemfree(saccade_params_estim{i}.end_index), 'v', ...
        'Color', [0.1 0.1 0.1], 'LineWidth', 1)
end

xlabel('$Time [ms]$', 'Interpreter', 'latex')
ylabel('$y^{Pulse}$ [g]', 'Interpreter', 'latex')
if strcmp(dataSource, 'generated')
    if strcmp(saccGenerationModel, 'Bahill1980')
        lgd = legend('Simulated pulse-step signal', 'Estimated pulse-step signal');
    end
else
    lgd = legend('Estimated pulse-step signal');
end
axis([0 t_max -200 200])
set(lgd, 'Interpreter', 'latex')
set(gca,'FontSize',13)
hold off

linkaxes(findobj(gcf,'type','axes'),'x')

end

