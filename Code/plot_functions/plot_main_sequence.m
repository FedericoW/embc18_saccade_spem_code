function plot_main_sequence(saccade_params_estim, saccade_params_true, dataSource)

subplot(2,1,1)
hold on
max_amplitude = 0;

if strcmp(dataSource, 'generated')
    for i=1:length(saccade_params_true)
        h1 = plot(abs(saccade_params_true{i}.step_size), ...
            abs(saccade_params_true{i}.v_max), '.', ...
            'Color', [0.8 0.6 0.4], 'MarkerSize', 15);

        % Maximal amplitude for plot boudaries:
        if abs(saccade_params_true{i}.step_size) > max_amplitude
            max_amplitude = abs(saccade_params_true{i}.step_size);
        end
    end
end

for i=1:length(saccade_params_estim)
    h2 = plot(abs(saccade_params_estim{i}.step_size), ...
        abs(saccade_params_estim{i}.v_max), '.', ...
        'Color', [0.1 0.1 0.1], 'MarkerSize', 6);
        
    % Maximal amplitude for plot boudaries:
    if abs(saccade_params_estim{i}.step_size) > max_amplitude
        max_amplitude = abs(saccade_params_estim{i}.step_size);
    end
end

x = linspace(0, max_amplitude, 100);
h3 = plot(x, 551 * (1 - exp(-x/14)), 'k');
xlabel('Amplitude $[^{\circ}]$', 'Interpreter', 'latex')
ylabel('Peak velocity $[^{\circ}/s]$', 'Interpreter', 'latex')
if strcmp(dataSource, 'generated')
    lgd = legend([h1 h2 h3], ...
        'Main sequence (simulated)', ...
        'Main sequence (detected)', ...
        'Main sequence (Baloh 1975)', ...
        'Location', 'northwest');
else
    lgd = legend([h2 h3], ...
        'Main sequence (detected)', ...
        'Main sequence (Baloh 1975)', ...
        'Location', 'northwest');
end
set(lgd, 'Interpreter', 'latex')
axis([0 max_amplitude 0 300])
set(gca,'FontSize',13)


subplot(2,1,2)
hold on

if strcmp(dataSource, 'generated')
    for i=1:length(saccade_params_true)
        h1 = plot(abs(saccade_params_true{i}.step_size), ...
            saccade_params_true{i}.duration * 1000, '.', ...
            'Color', [0.8 0.6 0.4], 'MarkerSize', 15);
    end
end

for i=1:length(saccade_params_estim)
    h2 = plot(abs(saccade_params_estim{i}.step_size), ...
        saccade_params_estim{i}.duration * 1000, '.', ...
        'Color', [0.1 0.1 0.1], 'MarkerSize', 6);
end

h3 = plot(x, 2.7 * x + 37, 'k');
xlabel('Amplitude $[^{\circ}]$', 'Interpreter', 'latex')
ylabel('Duration $[ms]$', 'Interpreter', 'latex')
if strcmp(dataSource, 'generated')
    lgd = legend([h1, h2, h3], ...
        'Main sequence (simulated)', ...
        'Main sequence (detected)', ...
        'Main sequence (Baloh 1975)', ...
        'Location', 'northwest');
else
    lgd = legend([h2, h3], ...
        'Main sequence (detected)', ...
        'Main sequence (Baloh 1975)', ...
        'Location', 'northwest');
end
set(lgd, 'Interpreter', 'latex')
axis([0 max_amplitude 15 60])
set(gca,'FontSize',13)

end

