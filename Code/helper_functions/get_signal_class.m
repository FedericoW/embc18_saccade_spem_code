function class_signal = get_signal_class(sacc_params, dtheta_spem, Ts)
%   @ class_signal: Signal of length n that classifies every sample of the 
%   observation by one of the classes:
%       - 1: Fixation
%       - 2: Saccade
%       - 3: Smooth Pursuit

% Threshold parameters:
min_spem_vel = 2;
min_spem_acc = 50;

n = length(dtheta_spem);

% Start with classifying everything as fixation:
class_signal = ones(1,n);

% Identify SPEM samples:
ddtheta_spem = gradient(dtheta_spem, Ts);
for k=1:n
    if abs(dtheta_spem(k)) > min_spem_vel || abs(ddtheta_spem(k)) > min_spem_acc
        class_signal(k) = 3;
    end
end

% Identify saccade samples:
for i=1:length(sacc_params)
    class_signal(sacc_params{i}.start_index : sacc_params{i}.end_index) = 2;
end

end

