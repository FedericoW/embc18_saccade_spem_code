function LSSM = load_LSSM(plantModelType, saccModelType, spemModelType, signals)

% Oculomotor plant model from Bahill 1980
% The continuous time model is discretized and a saccade and SPEM model is
% added resulting in one large linear state space model (LSSM)

LSSM.lenRecording = signals.lenRecording;
LSSM.Ts = 1 / signals.samplingRate;

if strcmp(plantModelType, 'Bahill1980')
    % Define occular plant model from Bahill 1980 (without 6th state):
    K_SE = 2.5;
    K_LT = 1.2;
    K_P = 0.5;
    B_P = 0.06;
    B_AG = 0.046;
    B_ANT = 0.022;
    J = 4.3e-5;
    tau_AG = 2e-3;
    A_cont = [0, 0, 0, 1, 0; ...
            K_SE^2/(K_LT+K_SE)/B_AG, -K_SE/B_AG, 0, 0, K_SE/(K_LT+K_SE)/B_AG; ...
            K_SE^2/(K_LT+K_SE)/B_ANT, 0, -K_SE/B_ANT, 0, 0; ...
            -(2*K_SE+K_P)/J, K_SE/J, K_SE/J, -B_P/J, 0; ...
            0, 0, 0, 0, -1/tau_AG];
    B_cont = [0; 0; 0; 0; 1/tau_AG];
    C_cont = [1, 0, 0, 0, 0];

    % Continous time model:
    LSSM.plant_model_cont = ss(A_cont,B_cont,C_cont,0);

    % Discrete time model:
    sys_disc = c2d(LSSM.plant_model_cont, LSSM.Ts);
    A_plant = sys_disc.A;
    B_plant = sys_disc.B;
    C_plant = C_cont;

    % Observation matrix for dtheta simulations:
    C_plant_vel = [0, 0, 0, 1, 0];
    LSSM.plant_model_cont_vel = ss(A_cont,B_cont,C_plant_vel,0);

    x_plant_dim = length(A_plant);

    % Initialize plant model parameters:
    m_X0_plant = zeros(x_plant_dim,1);
    V_X0_plant = zeros(x_plant_dim);
    V_X0_plant(1,1) = 10;
    V_X0_plant(2,2) = 10;
    V_X0_plant(3,3) = 10;
    V_X0_plant(5,5) = 50;
end

% Define saccade model:
if strcmp(saccModelType, 'pulseStep')
    A_sacc = 1;
    B_sacc = 1;
    C_sacc = 1;
    x_sacc_dim = 1;
    u_sacc_dim = 1;
    
    % Initialize parameters:
    m_X0_sacc = zeros(x_sacc_dim,1);
    V_X0_sacc = zeros(x_sacc_dim);
    V_X0_sacc(1,1) = 50;
    
    m_U_sacc = zeros(u_sacc_dim, LSSM.lenRecording);
    V_U_sacc = zeros(u_sacc_dim, u_sacc_dim, LSSM.lenRecording);
    V_U_sacc(1,1,:) = 100^2;
end

if strcmp(spemModelType, 'integratedSine')
    LSSM.f_spem = signals.f_spem;
    
    A_spem = ...
        [1, LSSM.Ts, 0;
        0, cos(2*pi*LSSM.f_spem*LSSM.Ts), sin(2*pi*LSSM.f_spem*LSSM.Ts); ...
        0, -sin(2*pi*LSSM.f_spem*LSSM.Ts), cos(2*pi*LSSM.f_spem*LSSM.Ts)];
    B_spem = zeros(3,0);
    C_spem = [1, 0, 0];
    
    C_spem_sin = [0, 1, 0];
    C_spem_cos = [0, 0, 1];
    
    x_spem_dim = length(A_spem);
    u_spem_dim = size(B_spem, 2);
    
    % Initialize parameters:
    m_X0_spem = zeros(x_spem_dim,1);
    V_X0_spem = zeros(x_spem_dim);
    
    m_U_spem = zeros(u_spem_dim, LSSM.lenRecording);
    V_U_spem = zeros(u_spem_dim, u_spem_dim, LSSM.lenRecording);
    
    
elseif strcmp(spemModelType, 'integratedRW')
    A_spem = [1, LSSM.Ts; 0, 1];
    B_spem = [0; 1];
    C_spem = [1, 0];
    
    C_spem_sin = [nan, nan];
    C_spem_cos = [nan, nan];
    
    x_spem_dim = length(A_spem);
    u_spem_dim = size(B_spem, 2);
    
    % Initialize control model parameters:
    m_X0_spem = zeros(x_spem_dim,1);
    V_X0_spem = zeros(x_spem_dim);
    
    m_U_spem = zeros(u_spem_dim, LSSM.lenRecording);
    V_U_spem = zeros(u_spem_dim, u_spem_dim, LSSM.lenRecording);
    V_U_spem(1,1,:) = 0;    
end

% Create LSSM:
LSSM.A = [A_plant, B_plant * [C_sacc, C_spem]; ...
    zeros(x_sacc_dim + x_spem_dim, x_plant_dim), blkdiag(A_sacc, A_spem)];
LSSM.B = [zeros(x_plant_dim, u_sacc_dim + u_spem_dim); blkdiag(B_sacc, B_spem)];
LSSM.C = [C_plant, zeros(1, x_sacc_dim + x_spem_dim)];

LSSM.C_vel = [C_plant_vel, zeros(1, x_sacc_dim + x_spem_dim)];
LSSM.C_control = [zeros(1, x_plant_dim), [C_sacc, C_spem]];
LSSM.C_fix = [zeros(1, x_plant_dim), C_sacc, zeros(1,x_spem_dim)];
LSSM.C_spem = [zeros(1, x_plant_dim), zeros(1,x_sacc_dim), C_spem];
LSSM.C_spem_sin = [zeros(1, x_plant_dim), zeros(1,x_sacc_dim), C_spem_sin];
LSSM.C_spem_cos = [zeros(1, x_plant_dim), zeros(1,x_sacc_dim), C_spem_cos];

LSSM.xdim = size(LSSM.A, 1);
LSSM.udim = size(LSSM.B, 2);
LSSM.ydim = 1;

% Initialize LSSM parameters:
LSSM.m_X0 = [m_X0_plant; m_X0_sacc; m_X0_spem];
LSSM.V_X0 = blkdiag(V_X0_plant, V_X0_sacc, V_X0_spem);

LSSM.m_U = [m_U_sacc; m_U_spem];
LSSM.V_U = zeros(LSSM.udim, LSSM.udim, LSSM.lenRecording);

for k=1:LSSM.lenRecording
    LSSM.V_U(:,:,k) = blkdiag(V_U_sacc(:,:,k), V_U_spem(:,:,k));
end

end

