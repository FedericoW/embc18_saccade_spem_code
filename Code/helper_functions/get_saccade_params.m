function saccade_params = get_saccade_params(pos, vel, acc, time, sparse_input)
%UNTITLED10 Summary of this function goes here
%   Calculate: 
%   saccade_params
%       .step_size
%       .duration
%       .v_max
%       .v_max_index
%       .start_index
%       .end_index

% Threshold parameters:
minimum_sacc_ampl = 0.1;
minimum_sacc_vel = 20;
minimum_sacc_acc = 1000;
sparsity_thres = 0.5;
max_sacc_len = 0.05;
acc_thres = 1000;
vel_thres = 10;

if exist('sparse_input', 'var')
    sacc_locations = (abs(sparse_input) > sparsity_thres) .* sparse_input;
else
    sacc_locations = (abs(acc) > minimum_sacc_acc) .* acc;
end

saccade_params = cell(0);
n = length(pos);
is_saccade = false;
cur_sacc_indices = zeros(20, 1);
cur_sacc_length = 0;
count = 0;
k = 1;
while k <= n
    if ~is_saccade
        if abs(sacc_locations(k)) > 0
            is_saccade = true;
            count = count + 1;
            saccade_params{count}.v_max = 0;
            saccade_params{count}.v_max_index = k;
            cur_sacc_indices(1) = k;
            cur_sacc_length = 1;
        end
    else
        if abs(vel(k)) > abs(saccade_params{count}.v_max)
            saccade_params{count}.v_max = vel(k);
            saccade_params{count}.v_max_index = k;
        end
        
        if abs(sacc_locations(k)) > 0
            cur_sacc_length = cur_sacc_length + 1;
            cur_sacc_indices(cur_sacc_length) = k;
        end
        
        if time(k) - time(cur_sacc_indices(cur_sacc_length)) > max_sacc_len ...
                || k == n
            cur_u = sacc_locations(1,cur_sacc_indices(1:cur_sacc_length));
            if sum(cur_u > 0) == cur_sacc_length || sum(cur_u < 0) == cur_sacc_length ...
                    || abs(saccade_params{count}.v_max) < minimum_sacc_vel
                
                % Acceleration only in one direction
                % or peak velocity < minimum_sacc_vel
                % ==> No saccade.
                count = count - 1;
                saccade_params = saccade_params(1:count);
                
            else % Saccade detected! Get parameters:
                l = saccade_params{count}.v_max_index;
                while (abs(acc(l)) > acc_thres ...
                        || abs(vel(l)) > vel_thres) ...
                        && l > cur_sacc_indices(1)
                    l = l - 1;
                end
                saccade_params{count}.start_index = l;
                
                l = saccade_params{count}.v_max_index;
                while ( abs(acc(l)) > acc_thres ...
                        || abs(vel(l)) > vel_thres ) ...
                        && l < n-1
                    l = l + 1;
                end
                saccade_params{count}.end_index = l;
                
                %saccade_params{count}.step_size = signals.y_hat(saccade_params{count}.end_index) ...
                %    - signals.y_hat(saccade_params{count}.start_index);
                
                saccade_params{count}.step_size = ...
                    pos(saccade_params{count}.end_index) ...
                    - pos(saccade_params{count}.start_index);
                
                %saccade_params{count}.v_max = signals.dy_hat(saccade_params{count}.v_max_index);
                
                saccade_params{count}.duration = ...
                    time(saccade_params{count}.end_index) ...
                    - time(saccade_params{count}.start_index);
                
                k = saccade_params{count}.end_index;
                
                if abs(saccade_params{count}.step_size) < minimum_sacc_ampl
                    % step_size < minimum_sacc_ampl
                    % ==> No saccade.
                    count = count - 1;
                    saccade_params = saccade_params(1:count);
                end
                
            end
            
            is_saccade = false;
            cur_sacc_length = 0;

        end
    end
    
    k = k + 1;
    
end

end

