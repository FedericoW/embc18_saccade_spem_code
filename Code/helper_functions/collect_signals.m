function signals = collect_signals(signals, LSSM)
%UNTITLED9 Summary of this function goes here
%   Return arguments:
%   signals
%       .dy
%       .y_butter
%       .dy_butter
%       .y_hat
%       .dy_hat
%       .u_saccades
%       .control
%       .control_spemfree
%       .control_spem
%       .y_spem
%       .y_spemfree
%       .dy_spem
%       .dy_spemfree
%       .ddy_spemfree

% % Smoothed derivative of data:
% dy = conv(gradient(signals.data, LSSM.Ts), gaussdesign(0.1, 10, 1));
% signals.dy = dy(6:length(dy)-5);

% Signals from butterworth filtering for comparison:
% [z,p,k] = butter(9, 40 * (2*LSSM.Ts));
% [sosbp,gbp] = zp2sos(z,p,k);
% signals.y_butter = filtfilt(sosbp, gbp, signals.data);
% signals.dy_butter = gradient(signals.y_butter, LSSM.Ts);

% Estimated position and velocity:
signals.y_hat = LSSM.C * signals.x_hat;
signals.dy_hat = LSSM.C_vel * signals.x_hat;

% Estimated control signals:
signals.u_saccades = signals.u_hat(1,:);
signals.control = LSSM.C_control * signals.x_hat;
signals.control_spemfree = LSSM.C_fix * signals.x_hat;
signals.control_spem = LSSM.C_spem * signals.x_hat;

% SPEM part of estimated position y_hat:
signals.y_spem = lsim(LSSM.plant_model_cont, signals.control_spem, signals.time)';

% SPEM-free part of estimated position y_hat:
signals.y_spemfree = signals.y_hat - signals.y_spem;

% SPEM part of estimated velocity dy_hat:
signals.dy_spem = lsim(LSSM.plant_model_cont_vel, signals.control_spem, signals.time)';

% SPEM-free part of estimated velocity dy_hat:
signals.dy_spemfree = signals.dy_hat - signals.dy_spem;

% Derivative of SPEM-free velocity dy_spemfree:
signals.ddy_spemfree = gradient(signals.dy_spemfree, LSSM.Ts);

end

