clear all;
close all;
clc;

% Run script from within the 'Code' folder
addpath(genpath(cd));

% Algorithm parameters:
plantModelType = 'Bahill1980';
saccModelType = 'pulseStep';
spemModelType = 'integratedSine'; % (Eq.(9) EMBC'18 Wadehn, F. et al.)
%spemModelType = 'integratedRW'; % (Random walk on velocity Eq.(8) EMBC'18 Wadehn, F. et al.)

% Load recoding:
load('../Recordings/recording_4', '-mat')
signals.data = data';
 
signals.lenRecording = length(signals.data);
signals.samplingRate = 1 / (time(2) - time(1)); % [Hz]
signals.a_spem = amplitude; % SPEM amplitude
signals.f_spem = frequenz; % SPEM frequency
signals.time = time; % time vector of recording
signals.target = signals.a_spem * sin(2*pi * signals.f_spem * signals.time);
signals.dtarget = signals.a_spem * 2*pi * signals.f_spem * cos(2*pi * signals.f_spem * signals.time);% Derivative of target SPEM signal
 
clear 'amplitude'
clear 'data'
clear 'frequenz'
clear 'samps'
clear 'sichtbar'
clear 'time'


%% Model-based Saccade and SPEM separation

if strcmp(spemModelType, 'integratedSine')
  % State noise initial guess  
  V_W_init = [0 0 0 0 0 0 0 (500 / signals.samplingRate)^2 (500 / signals.samplingRate)^2];
elseif strcmp(spemModelType, 'integratedRW')
  % State noise initial guess 
  V_W_init = [0 0 0 0 0 0 0 (1000 / signals.samplingRate)^2];
end

[signals, LSSM, saccParamsEstim] = model_based_saccade_spem_detection(plantModelType, ...
                                                            saccModelType, ...
                                                            spemModelType, ...
                                                            signals, ...
                                                            'U_priors', {'jeffreys_prior', 'jeffreys_prior'}, ...
                                                            'V_W_init', V_W_init, ...
                                                            'V_W_updates', [2,3,4,5], ...
                                                            'V_W_prior', 'jeffreys_prior', ...
                                                            'V_W_prior_args', 0.1, ...
                                                            'V_Z_init', 0.05^2, ...
                                                            'V_Z_update', true, ...
                                                            'V_Z_prior', 'no_prior','verbose', true);
                                                        
% For invgamma_prior instead of jeffreys prior use additional argument: 'U_prior_args', [0, 0.000001; 0, 0.000001]                                                       


%% Evaluation Plots

%%% 1.) Signal separation overview plot:
figure(1);
plot_separation_overview(signals);

%%% 2.) Saccade plot:
figure(2);
plot_saccades(signals, [], saccParamsEstim, 'sinusTarget');

%%% 3.) Main sequence plot:
figure(3);
plot_main_sequence(saccParamsEstim, [], 'sinusTarget');

%%% 4.) Spem analysis plot:
figure(4);
if strcmp(spemModelType, 'integratedSine')
  plot_spem_analysis(signals, LSSM, spemModelType, signals.a_spem, signals.f_spem);
elseif strcmp(spemModelType, 'integratedRW')
  plot_spem_analysis(signals, LSSM, spemModelType)
end
